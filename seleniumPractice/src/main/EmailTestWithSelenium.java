package main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/*
Name:Robel Fekadu
ID:ATR/7329/09
Section:3
 */
public class EmailTestWithSelenium {
    public static void main(String[] args) throws InterruptedException, IOException {
        System.setProperty("webdriver.chrome.driver", "/home/robi/ubuntu/chromedriver_linux64/chromedriver");

        // Create a new instance of the Chrome driver
        WebDriver driver = new ChromeDriver();

        //open gmail site
        driver.get("http://www.gmail.com/");

        //find the email text box and enter email
        driver.findElement(By.id("identifierId")).sendKeys("test@gmail.com");

        //click  next
        driver.findElement(By.id("identifierNext")).click();

        //wait till the page loads
        Thread.sleep(7000L);

        //find password text box and enter password
        driver.findElement(By.name("password")).sendKeys("password");

        //click next
        driver.findElement(By.id("passwordNext")).click();

        //wait till the page loads
        Thread.sleep(20000L);

        //find list of unread messages
        List<WebElement> unreadMails=driver.findElements(By.xpath("//*[@class='zA zE']"));


        FileWriter fileWriter = new FileWriter("UnreadMessages.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for(WebElement unreadMail: unreadMails){
            printWriter.append(unreadMail.getText() + "\n");
        }
        printWriter.close();

        System.out.println("No of unread mails are:" + unreadMails.size());

        driver.close();
    }
}
